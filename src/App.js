import { useState, useEffect } from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap";
import './App.css';
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Books from "./pages/Books";
import Users from "./pages/Users";
import Error from "./pages/Error";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Orders from "./pages/Orders";



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: "POST",
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/books/all" element={<Books />} />
            <Route exact path="/users/all" element={<Users />} />
            <Route exact path="/orders/all" element={<Orders />} />
            
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<Error />} />
          </Routes>
            
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
