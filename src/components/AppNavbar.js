import { useContext } from "react"
import { NavLink } from "react-router-dom";

import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){

    const { user } = useContext(UserContext);
    console.log(user);
    return(
        <Navbar className="navbar" expand="lg">
        <Container>
            <Navbar.Brand className="text-white " style={{fontSize:"20px"}} as={ NavLink } to="/">DigiBooks</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
                <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/" end>Home</Nav.Link>
                <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/books/all" end>Books</Nav.Link>
                <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/users/all" end>Users</Nav.Link>
                <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/orders/all" end>Orders</Nav.Link>
                {
                    (user.id !== null)
                    ?
                        <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/logout" end>Logout</Nav.Link>
                    :
                    <>
                        <Nav.Link className="text-white mx-3" style={{fontSize:"18px"}} as={ NavLink } to="/login" end>Login</Nav.Link>
                    </>
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}