import Banner from "../components/Banner";

export default function Home(){
	const data = {
		title: "DigiBooks",
		content: '"There is no friend as loyal as a book" ',
		author: "by Ernest Hemingway",
		destination: "/login",
		label: "Sell Like Crazy!"
	}

	return(
		<>	
			<Banner bannerProp={data}/>
		</>
	)
}