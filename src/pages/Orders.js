import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form} from "react-bootstrap"
import Swal from "sweetalert2";
import AppNavbar from "../components/AppNavbar";
import React from "react";

export default function Orders() {


const { user } = useContext(UserContext);

    const [allOrders, setAllOrders]= useState([]);

    const [bookId, setBookId] = useState("");
    const [quantity, setQuantity] = useState("");
    const [purchasedOn, setPurchasedOn] = useState("");


    const [isActive, setIsActive] = useState(false);






	/**********[GET ALL USERS]**********/
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/getAllOrders`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllOrders(data.map(orders =>{

                return (
                    <>
                    <tr key={orders._id}>
                        <td>{orders.bookId}</td>
                        <td>{orders.quantity}</td>
                        <td>{orders.purchasedOn}</td>
                        {/*<td>
                            {(users.isAdmin !== true)
                            ?
                            <>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(users._id)}>Edit</Button>
                            <Button variant="success" className="mx-1" size="sm" onClick={() => unarchive(users._id, users.name)}>Unarchive</Button>
                            <Button variant="danger" size="sm" onClick={() => archive(users._id, users.name)}>Archive</Button>
                            </>
                            :
                            ''
                        }
                        </td>*/}
                    </tr>
                    </>
                )
            }));
		});
	}


    return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
       
        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={12}>
        
        {
        (user.isAdmin)
        ?
        <>
        <div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
            <div className="my-2 text-center">
                <h1>VIEW ALL ORDERS</h1>
            </div>
            <Table striped bordered hover className="shadow-lg shadow-sm ">
            <thead className="banner-bg">
                <tr>
                <th>User ID</th>
                <th>Book ID</th>
                <th>Quantity</th>
                <th>Purchased Date</th>
                <th>Status</th>
                <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {allOrders}
            </tbody>
            </Table>
        </div>
        </>
        :
        <Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>

    </>

    )

};

