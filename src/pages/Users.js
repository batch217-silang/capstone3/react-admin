import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form} from "react-bootstrap"
import Swal from "sweetalert2";
import AppNavbar from "../components/AppNavbar";
import React from "react";

export default function Users() {


const { user } = useContext(UserContext);

    const [allUsers, setAllUsers]= useState([]);

    const [userId, setUserId] = useState("");
    const [userName, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const [isAdmin, setIsAdmin] = useState(false);


    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(false);


    const addUserOpen = () => setModalAdd(true);
    const addUserClose = () => setModalAdd(false);

    const openEdit = (id) => {
        setUserId(id);

        fetch(`${process.env.REACT_APP_API_URL}/users/${id}/update`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

	                setUserName(data.userName);
	                setEmail(data.email);
	                setPassword(data.password);
                    setIsAdmin(data.isAdmin);
            });

     	setModalEdit(true)
    };

    const closeEdit = () => {

        setUserName('');
        setEmail('');
        setPassword('');
        setIsAdmin('');

     setModalEdit(false);
    };




	/**********[GET ALL USERS]**********/
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllUsers(data.map(users =>{

                return (
                    <>
                    <tr key={users._id}>
                        <td>{users._id}</td>
                        <td>{users.userName}</td>
                        <td>{users.email}</td>
                        <td>{users.password ? "password" : "password"}</td>
                        <td>{users.isAdmin ? "Admin" : "Customer"}</td>
                        <td>
                            {(users.isAdmin !== true)
                            ?
                            <>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(users._id)}>Edit</Button>
                            <Button variant="success" className="mx-1" size="sm" onClick={() => unarchive(users._id, users.name)}>Unarchive</Button>
                            <Button variant="danger" size="sm" onClick={() => archive(users._id, users.name)}>Archive</Button>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(users._id)}>Orders</Button>
                            </>
                            :
                            ''
                        }
                        </td>

                    </tr>
                    </>
                )
            }));
		});
	}


    /**********[ARCIVE A USER]**********/
    const archive = (id) =>{
        // console.log(id);
        // console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/users/${id}/archive`,
        	{
	            method : "PUT",
	            headers : {
	                "Content-Type" : "application/json",
	                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "ARCHIVING SUCCESSFULL!",
                    icon: "success",
                    text: `Now in archive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "ARCHIVING UNSUCCESSFULL!",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unarchive = (id) =>{
        // console.log(id);
        // console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/users/${id}/archive`,
        	{
	            method : "PUT",
	            headers : {
	                "Content-Type" : "application/json",
	                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: true
        })
    
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "UNARCHIVING SUCCESSFULL!",
                    icon: "success",
                    text: `Inactive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unarchive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

	useEffect(()=>{
		fetchData();
	}, [])


	return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
       
        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={12}>
        
        {
        (user.isAdmin)
		?
        <>
		<div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
			<div className="mb-5 text-center">
				<h1>VIEW ALL USERS</h1>
			</div>
            <Table striped bordered hover className="shadow-lg shadow-sm ">
            <thead className="banner-bg">
                <tr className="text-center">
                <th>USER ID</th>
                <th>USER NAME</th>
                <th>EMAIL</th>
                <th>PASSWORD</th>
                <th>STATUS</th>
                <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                {allUsers}
            </tbody>
            </Table>
		</div>
        </>
		:
		<Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>

    </>

	)

};



