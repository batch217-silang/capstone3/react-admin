import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form} from "react-bootstrap"
import Swal from "sweetalert2";
import AppNavbar from "../components/AppNavbar";
import React from "react";

export default function Library() {


const { user } = useContext(UserContext);

    const [allBooks, setAllBooks]= useState([]);

    const [bookId, setBookId] = useState("");
    const [title, setTitle] = useState("");
    const [author, setAuthor] = useState("");
    const [genre, setGenre] = useState("");
    const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);

    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(false);


    const addBookOpen = () => setModalAdd(true);
    const addBookClose = () => setModalAdd(false);

    const openEdit = (id) => {
        setBookId(id);

        fetch(`${process.env.REACT_APP_API_URL}/books/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                    setTitle(data.title);
                    setAuthor(data.author);
                    setGenre(data.genre);
                    setPrice(data.price);
            });

        setModalEdit(true)
    };

    const closeEdit = () => {

        setTitle('');
        setAuthor('');
        setGenre('');
        setPrice(0);

     setModalEdit(false);
    };




    /**********[GET ALL BOOKS]**********/
    const fetchData = () =>{
        fetch(`${process.env.REACT_APP_API_URL}/books/all`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllBooks(data.map(books =>{

                return (
                    <>
                    <tr key={books._id}>
                        <td>{books._id}</td>
                        <td>{books.title}</td>
                        <td>{books.author}</td>
                        <td>{books.genre}</td>
                        <td>{books.price}</td>
                        <td>{books.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {(books.isActive)
                            ?
                            <Button variant="danger" size="sm" onClick={() => archive(books._id)}>Archive</Button>
                            :
                            <>
                            <Button variant="success" className="mx-1" size="sm" onClick={() => unarchive(books._id)}>Unarchive</Button>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(books._id)}>Edit</Button>
                            
                            </>}
                        </td>
                    </tr>
                    </>
                )
            }));
        });
    }


    /**********[ARCIVE A BOOK]**********/
    const archive = (id) =>{
        // console.log(id);
        // console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/books/${id}/archive`,
            {
                method : "PATCH",
                headers : {
                    "Content-Type" : "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "ARCHIVING SUCCESSFULL!",
                    icon: "success",
                    text: `Now in archive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "ARCHIVING UNSUCCESSFULL!",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unarchive = (id) =>{
        // console.log(id);
        // console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/books/${id}/archive`,
            {
                method : "PATCH",
                headers : {
                    "Content-Type" : "application/json",
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: true
        })
    
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "UNARCHIVING SUCCESSFULL!",
                    icon: "success",
                    text: `Inactive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unarchive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    useEffect(()=>{
        fetchData();
    }, [])


    /**********[ADD A BOOK]**********/
    const addBook = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/books/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                title: title,
                author: author,
                genre: genre,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "ADDED SUCCESSFULLY!",
                        icon: "success",
                        text: `"New book is added to the library.`,
                    });


                    fetchData();
                    addBookClose();
                }
                else {
                    Swal.fire({
                        title: "UNSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });
                    addBookClose();
                }

            })

        setTitle('');
        setAuthor('');
        setGenre('');
        setPrice(0);
    }

    /**********[EDIT A BOOK]**********/
    const editBook = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/books/${bookId}/update`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                title: title,
                author: author,
                genre: genre,
                price: price
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "EDIT SUCCESSFUL!",
                        icon: "success",
                        text: `Book was edited successfully.`,
                    });

                    fetchData();
                    closeEdit();

                }
                else {
                    Swal.fire({
                        title: "UNSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });

                    closeEdit();
                }

            })

        setTitle('');
        setAuthor('');
        setGenre('');
        setPrice(0);
    }

    useEffect(() => {

        if (title != "" && author != "" && genre != "" && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [title, author, genre, price]);




    return(
        <>
         
        <Container fluid className="w-100 d-flex h-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
       
        <Col className="colr-bg m-0 p-5 d-flex justify-content-center" xs={12} md={10} lg={12}>
        {
            (user.isAdmin)
        ?
        <>
        <div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5">
            <div className="mb-5 text-center">
                <h1>VIEW ALL BOOKS</h1>
            </div>
                
            <Table striped bordered hover className="shadow-lg shadow-sm ">
            <thead className="banner-bg">
                <tr className="text-center">
                <th>BOOK ID</th>
                <th>TITLE</th>
                <th>AUTHOR</th>
                <th>GENRE</th>
                <th>PRICE</th>
                <th>STATUS</th>
                <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                {allBooks}
            </tbody>
            </Table>
            <div className="my-2 text-right">
                    <Button  variant="success" className="px-5" onClick={addBookOpen}>ADD A BOOK</Button>
                </div>
             

                 {/* ADD BOOK MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalAdd}
                >
                <Form onSubmit={e => addBook(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>ADD BOOK</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                        <Row>
                        <Col xs={6} md={6} lg={6}>
                            <Form.Group controlId="title" className="mb-3">
                                <Form.Label>Title</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Book Title"
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="author" className="mb-3">
                                <Form.Label>Author</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Book Author"
                                    value={author}
                                    onChange={e => setAuthor(e.target.value)}
                                    required
                                />
                            </Form.Group>


                            <Form.Group controlId="genre" className="mb-3">
                                <Form.Label>Genre</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Book Genre"
                                    value={genre}
                                    onChange={e => setGenre(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Book Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            </Col>
                            </Row>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    ADD BOOK
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    ADD BOOK
                                </Button>
                            }
                            <Button variant="secondary" onClick={addBookClose}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>
                
                {/* EDIT MODAL */}

                <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={modalEdit}
                >
                <Form onSubmit={e => editBook(e)}>

                        <Modal.Header className="banner-bg text-light">
                            <Modal.Title>EDIT BOOK</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form.Group controlId="title" className="mb-3">
                                <Form.Label>Title</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Book Title"
                                    value={title}
                                    onChange={e => setTitle(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="author" className="mb-3">
                                <Form.Label>Author</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Book Author"
                                    value={author}
                                    onChange={e => setAuthor(e.target.value)}
                                    required
                                />
                            </Form.Group>


                            <Form.Group controlId="genre" className="mb-3">
                                <Form.Label>Genre</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter Book Genre"
                                    value={genre}
                                    onChange={e => setGenre(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Book Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Book Price"
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeEdit}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>



        </div>
        </>
        :
        <Navigate to="/" />}

        </Col>
        
        </Row>
    </Container>

    


    </>


    )
}
